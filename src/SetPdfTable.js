import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
pdfMake.vfs = pdfFonts.pdfMake.vfs;

export class SetPdfTable {
  constructor(o) {
    this.table = o.table;
    this.format_dates = o.format_dates;
    this.tasks = o.tasks;
    this.pdf_width = this.table.offsetWidth;
    this.pdf_height = this.table.offsetHeight;
    this.day_width = o.day_width;
    this.Init();
  }
  Init() {
    this.CreateTableHead();
    this.CreateTableBody();
    this.generateReport();
  }
  CreateTableBody() {
    this.table_body = [];
    this.tasks.forEach((row) => {
      let dataRow = [];
      dataRow.push(row.name);
      let start = row.style_left / this.day_width;
      let task_dur = 0;
      this.format_dates.forEach((elem, index) => {
        task_dur = Math.ceil(row.style_width / this.day_width);
        if (index >= start && index < task_dur + start) {
          dataRow.push({
            text: "",
            style: { fillColor: row.color },
          });
        } else {
          dataRow.push("");
        }
      });
      this.table_body.push(dataRow);
    });
  }
  CreateTableHead() {
    this.head_dates = [];
    this.col_width = [];
    this.head_dates.push(" ");
    this.col_width.push("*");

    for (let i = 0; i < this.format_dates.length; i++) {
      this.head_dates.push(this.format_dates[i]);
      this.col_width.push("*");
    }
  }
  generateReport() {
    let t = this;

    this.pdf_content = {
      pageSize: {
        width: t.pdf_width,
        height: t.pdf_height,
      },
      pageOrientation: "landscape",
      pageMargins: [0, 0, 0, 0],
      content: [
        {
          style: "tableExample",
          table: {
            widths: t.col_width,
            body: [t.head_dates],
          },
        },
        {
          style: "tableExample",
          table: {
            widths: t.col_width,
            body: t.table_body,
          },
        },
      ],
    };
    pdfMake.createPdf(t.pdf_content).download("TaskTable.pdf");
  }
}
